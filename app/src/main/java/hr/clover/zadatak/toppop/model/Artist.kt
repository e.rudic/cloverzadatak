package hr.clover.zadatak.toppop.model

import com.google.gson.annotations.SerializedName

class Artist(
    @SerializedName("name")
    val name: String
) {
}