package hr.clover.zadatak.toppop

import MyTrackListRecyclerViewAdapter
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import hr.clover.zadatak.toppop.model.APIResponse
import hr.clover.zadatak.toppop.model.TrackInfo
import hr.clover.zadatak.toppop.service.APIService.Companion.APIServe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class MainActivity : AppCompatActivity(), SwipeRefreshLayout.OnRefreshListener {

    var recyclerView: RecyclerView? = null;
    var tracks: List<TrackInfo> = emptyList();
    var disposable: CompositeDisposable = CompositeDisposable();
    var swipeRefreshLayout: SwipeRefreshLayout? = null;

    override fun onRefresh() {
        loadRecyclerViewData()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initializeAdapter()
        loadRecyclerViewData()

        swipeRefreshLayout = findViewById(R.id.swipe_container);
        swipeRefreshLayout?.setOnRefreshListener {
            loadRecyclerViewData()
        }
    }

    fun loadRecyclerViewData() {
        disposable.add(
            APIServe.getChart().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::success, this::onError)
        )
    }

    fun initializeAdapter() {
        val myAdapter =
            MyTrackListRecyclerViewAdapter(this, tracks.toMutableList())
        recyclerView = findViewById(R.id.recyclerView)
        recyclerView?.adapter = myAdapter
        recyclerView?.layoutManager = LinearLayoutManager(this)
    }

    fun success(apiResponse: APIResponse) {
        tracks = apiResponse.tracks.data
        val myAdapter = recyclerView!!.adapter as MyTrackListRecyclerViewAdapter
        myAdapter.clear()
        myAdapter.addAll(apiResponse.tracks.data)
        swipeRefreshLayout?.isRefreshing = false
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.normal -> {
                tracks = tracks.sortedBy {trackInfo -> trackInfo.position }
            }

            R.id.sort_asc -> {
                tracks = tracks.sortedBy { trackInfo -> trackInfo.songLength }
            }

            R.id.sort_desc -> {
                tracks = tracks.sortedBy { trackInfo -> trackInfo.songLength }.reversed()
            }
        }
        refreshAdapter()
        return true
    }

    fun refreshAdapter(){
        val myAdapter = recyclerView!!.adapter as MyTrackListRecyclerViewAdapter
        myAdapter.clear()
        myAdapter.addAll(tracks)
    }

    fun onError(throwable: Throwable) {
    }
}

