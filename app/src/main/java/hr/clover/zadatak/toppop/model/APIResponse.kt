package hr.clover.zadatak.toppop.model

import com.google.gson.annotations.SerializedName

class APIResponse(
    @SerializedName("tracks")
    val tracks: Tracks
) {
}