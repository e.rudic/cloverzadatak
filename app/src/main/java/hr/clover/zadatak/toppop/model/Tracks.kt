package hr.clover.zadatak.toppop.model

import com.google.gson.annotations.SerializedName

class Tracks(
    @SerializedName("data")
    val data: List<TrackInfo>
) {

    override fun toString(): String {
        var string = ""
        for(i in data.indices){
            string += "${i+1}. ${data[i].songName}\n"
        }
        return string;
    }
}