import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import hr.clover.zadatak.toppop.AlbumActivity
import hr.clover.zadatak.toppop.R
import hr.clover.zadatak.toppop.model.TrackInfo
import io.reactivex.disposables.CompositeDisposable

const val ALBUM_ID = "hr.clover.zadatak.toppop.ALBUMID"
const val SONG_NAME = "hr.clover.zadatak.toppop.SONGNAME"


class MyTrackListRecyclerViewAdapter(
    val ct: Context,
    var tracks: MutableList<TrackInfo>
) : RecyclerView.Adapter<MyTrackListRecyclerViewAdapter.MyViewHolder>() {

    class MyViewHolder(
        private val myAdapter: MyTrackListRecyclerViewAdapter,
        itemView: View,
        val position: TextView = itemView.findViewById(R.id.position)
        , val songName: TextView = itemView.findViewById(R.id.songName)
        , val artistName: TextView = itemView.findViewById(R.id.artistName)
        , val length: TextView = itemView.findViewById(R.id.length)
    ) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        init {
            itemView.setOnClickListener(this);
        }

        override fun onClick(v: View?) {
            val intent = Intent(myAdapter.ct, AlbumActivity::class.java).apply {
                putExtra(ALBUM_ID, myAdapter.tracks[adapterPosition].album.id.toString())
                putExtra(SONG_NAME, myAdapter.tracks[adapterPosition].songName)
            }
            myAdapter.ct.startActivity(intent);
        }
    }

    override fun getItemCount(): Int {
        return tracks.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        //  Picasso.get().load(tracks[position].album.cover).into(holder.albumImg)
        holder.position.text = tracks[position].position.toString()
        holder.songName.text = tracks[position].songName
        holder.artistName.text = tracks[position].artist.name
        holder.length.text = tracks[position].songLength.toMMSSString()
    }

    fun Int.toMMSSString(): String {
        val minutes = this / 60
        val seconds = this % 60
        return String.format("%02d:%02d", minutes, seconds)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val inflater: LayoutInflater = LayoutInflater.from(ct)
        val view = inflater.inflate(R.layout.tracks, parent, false)
        return MyViewHolder(this, view)
    }

    fun clear() {
        tracks.clear()
        notifyDataSetChanged()
    }

    fun addAll(tracks: List<TrackInfo>) {
        this.tracks.addAll(tracks)
        notifyDataSetChanged()
    }
}