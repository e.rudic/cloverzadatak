package hr.clover.zadatak.toppop.model

import com.google.gson.annotations.SerializedName

class Album(
    @SerializedName("id")
    val id: Long,
    @SerializedName("title")
    val name: String,
    @SerializedName("artist")
    val artist: Artist,
    @SerializedName("cover_big")
    val coverUri: String,
    @SerializedName("tracks")
    val tracks: Tracks
) {
}