package hr.clover.zadatak.toppop.model

import com.google.gson.annotations.SerializedName
import java.text.FieldPosition

class TrackInfo(
    @SerializedName("position")
    val position: Int,
    @SerializedName("title")
    val songName: String,
    @SerializedName("artist")
    val artist: Artist,
    @SerializedName("duration")
    val songLength: Int,
    @SerializedName("album")
    val album: Album
) {

}