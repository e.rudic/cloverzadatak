package hr.clover.zadatak.toppop.service

import hr.clover.zadatak.toppop.model.APIResponse
import hr.clover.zadatak.toppop.model.Album
import hr.clover.zadatak.toppop.model.TrackInfo
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path


interface APIService {

    @GET("chart")
    fun getChart(): Single<APIResponse>

    @GET("album/{album_id}")
    fun getAlbum(@Path("album_id") albumId : Int) : Single<Album>

    companion object {
        val APIServe by lazy{
            APIService.create();
        }

        fun create(): APIService {
        val retrofit = Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("https://api.deezer.com/")
            .build()

        return retrofit.create(APIService::class.java)
    }}
}
