package hr.clover.zadatak.toppop

import ALBUM_ID
import SONG_NAME
import android.media.Image
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import hr.clover.zadatak.toppop.model.Album
import hr.clover.zadatak.toppop.service.APIService.Companion.APIServe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class AlbumActivity : AppCompatActivity() {
    var album: Album? = null;
    var songName: String = ""
    var disposable: CompositeDisposable = CompositeDisposable()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_album)

        val albumId = intent.getStringExtra(ALBUM_ID)?.toInt()
        songName = intent.getStringExtra(SONG_NAME)?: "";

        if (albumId != null) {
            disposable.add(APIServe.getAlbum(albumId).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::success, this::fail))
        }
    }

    fun success(album: Album) {
        this.album = album;
        loadViews()
    }

    fun loadViews(){
        if(album!=null){
            Picasso.get()
                .load(album?.coverUri)
                .into(findViewById<ImageView>(R.id.albumImg))
            findViewById<TextView>(R.id.artistName).text= album?.artist?.name
            findViewById<TextView>(R.id.albumName).text=album?.name
            findViewById<TextView>(R.id.songName).text=songName
            findViewById<TextView>(R.id.songNames).text = album?.tracks.toString()
        }
    }

    fun fail(throwable: Throwable) {

    }
}
